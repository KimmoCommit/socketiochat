var app = require('express')();	
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var express = require('express')
app.get('/', function(req, res){

	res.sendFile(__dirname +  "\\client\\views\\index.html");

});

app.use('/client', express.static(path.join(__dirname, '/client')));


io.on('connection', function(socket){
	var adr = socket.id;

	var connections = io.sockets.sockets.map(function(e) {
		return e.id;
	});

	console.log(connections);

	console.log("User with the IP: " + adr +  " connected.");

	io.sockets.emit("new connection", adr);
	io.sockets.emit("refresh people", connections);

	

	socket.on('disconnect', function(){
		var connections = io.sockets.sockets.map(function(e) {
			return e.id;
		});
		console.log(adr + " disconnected");
		io.sockets.emit("refresh people", connections);
		io.sockets.emit("disconnection", adr);
	});

	socket.on('chat message', function(msg){
		console.log(adr + ': ' + msg);

		var details = {
			'msg':msg,
			'adr':adr
		}

		io.emit('chat message', details);
	});

});


http.listen(3000, function(){
	console.log('3000 port is now being listened!! ;D')
});